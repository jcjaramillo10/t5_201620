package taller.mundo;

import taller.estructuras.*;


public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	// TODO
	private MaxHeap pedidosRecibidos; 

 	/** 
	 * Heap de elementos por despachar
	 */
	// TODO 
	private MinHeap pedidosDespachados;

	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		// TODO 
		pedidosRecibidos = new MaxHeap(100);
		pedidosDespachados = new MinHeap(100);
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		// TODO 
		pedidosRecibidos.add(new Pedido(precio, nombreAutor, cercania, true));
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO 
		Pedido p = pedidosRecibidos.poll();
		p.setRecibido(false);
		pedidosDespachados.add(p);
		return p;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO 
	    return pedidosDespachados.poll();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        // TODO 
        Pedido [] lista = new Pedido[pedidosRecibidos.size()];
        int j = 0;
        while (!pedidosRecibidos.isEmpty()){
        	lista[j++] = pedidosRecibidos.poll();
        }
        for (int i =0;i<lista.length; i++)
        {
        	pedidosRecibidos.add(lista[i]);
        }
    	 return lista;
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
         // TODO 
    	 Pedido [] lista = new Pedido[pedidosDespachados.size()];
         int j = 0;
         while (!pedidosDespachados.isEmpty()){
         	lista[j++] = pedidosDespachados.poll();
         }
         for (int i =0;i<lista.length; i++)
         {
         	pedidosDespachados.add(lista[i]);
         }
     	 return lista;
     }
}
