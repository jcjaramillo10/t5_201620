package taller.mundo;

public class Pedido implements Comparable<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	/**
	 * Esta en recibido
	 */
	private boolean recibido;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Pedido(double precio, String autorPedido, int cercania, boolean recibido) {

		this.precio = precio;
		this.autorPedido = autorPedido;
		this.cercania = cercania;
		this.recibido = recibido;
	}
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	

	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}
	
	// TODO 
	
	public boolean isRecibido() {
		return recibido;
	}

	public void setRecibido(boolean recibido) {
		this.recibido = recibido;
	}

	/**
	 * Realiza la comparacion por precio
	 * Si a es mas caro que b entonces a.compareTo(b) > 0
	 * Si a es mas cerca que b entonces a.compareTo(b) < 0
	 */
	@Override
	public int compareTo(Pedido o) {
		// TODO Auto-generated method stub
		if (this.recibido)
			return Double.compare( this.precio, o.precio);
		else
			return Integer.compare(cercania, o.cercania);
	}


}
